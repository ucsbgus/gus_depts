Requirements
------
* display a page listing departments
    * display the departments in alpha order
    * each dept has a link to detail page
* linked dept detail page shows url of server
* invalid url returns 404
    * 404 page is styled
* REST API
* REST API has authentication

* Application is
    * portable
    * tested

Model
---

    Department
      full_name = models.CharField(max_length=150)
      abbreviation = models.CharField(max_length=5)
      web_url = models.URLField(max_length=200, blank=True)
      server_uri = models.URLField(max_length=200)

Restarting
----------
This application is presently served from gdev2. To restart (it's a very 'hands-on' process right now), go to gdev2 

      $ workon depts
      $ python manage.py runserver 0.0.0.0:8086

Leave that terminal window open, or just start the process in the background