# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('common_name', models.CharField(max_length=150)),
                ('accounting_code', models.CharField(max_length=4)),
                ('abbreviation', models.CharField(max_length=4)),
                ('web_url', models.URLField()),
                ('server_uri', models.URLField()),
            ],
        ),
    ]
