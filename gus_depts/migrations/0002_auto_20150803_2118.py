# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gus_depts', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='department',
            old_name='common_name',
            new_name='full_name',
        ),
    ]
