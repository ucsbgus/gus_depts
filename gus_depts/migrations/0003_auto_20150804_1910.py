# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gus_depts', '0002_auto_20150803_2118'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='department',
            name='accounting_code',
        ),
        migrations.AlterField(
            model_name='department',
            name='web_url',
            field=models.URLField(null=True),
        ),
    ]
