# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gus_depts', '0003_auto_20150804_1910'),
    ]

    operations = [
        migrations.AlterField(
            model_name='department',
            name='web_url',
            field=models.URLField(blank=True),
        ),
    ]
