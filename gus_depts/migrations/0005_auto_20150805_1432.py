# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gus_depts', '0004_auto_20150804_1928'),
    ]

    operations = [
        migrations.AlterField(
            model_name='department',
            name='abbreviation',
            field=models.CharField(max_length=5),
        ),
    ]
