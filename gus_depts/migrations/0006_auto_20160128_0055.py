# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gus_depts', '0005_auto_20150805_1432'),
    ]

    operations = [
        migrations.AlterField(
            model_name='department',
            name='web_url',
            field=models.CharField(max_length=200, blank=True),
        ),
    ]
