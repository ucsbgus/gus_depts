from django.db import models

class Department(models.Model):
    full_name = models.CharField(max_length=150)
    abbreviation = models.CharField(max_length=5)
    # TODO set this as null=true and do not let blanks in the database
    web_url = models.CharField(max_length=200, blank=True)
    server_uri = models.URLField(max_length=200)

    def __str__(self):
        return '%s (%s)' % (self.full_name, self.abbreviation)
