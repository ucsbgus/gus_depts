from .models import Department
from rest_framework import serializers


class DeptSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Department
        fields = ('url','full_name', 'abbreviation', 'web_url', 'server_uri')
