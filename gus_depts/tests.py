from django.test import TestCase
from django.core.urlresolvers import reverse
from .views import detail

class ViewsTest(TestCase):
    fixtures = ['gus_depts']
    maxDiff = None

    def test_home_page_displays_depts(self):
        response = self.client.get('/')
        self.assertContains(response,'NANF')
        self.assertContains(response,'ERI')
        self.assertContains(response,'BREN')

    def test_home_page_displays_in_alpha_order(self):
        response = self.client.get('/')
        bren_index = response.content.index(b'BREN')
        eri_index  = response.content.index(b'ERI')
        nanf_index = response.content.index(b'NANF')
        self.assertTrue(0 < bren_index < eri_index < nanf_index)

    def test_detail_page_displays_uri(self):
        response = self.client.get('/depts/2/')
        self.assertContains(response, "NANF")
        self.assertContains(response, "http://nfl-gus.school.edu:19816")

    def test_invalid_url_returns_404(self):
        response = self.client.get('/depts/299/')
        self.assertEqual(response.status_code, 404)

class ModelTest(TestCase):
    # department has a attribute of 'created time' that works as expected

    # department has an 'updated time' attribute that works as expected

    # department has an 'updated by' field which says who did the last update

    pass

class APITest(TestCase):
    # there is a client that binds to the /api uri
    # client is presented with...

    # option to log in

    # authenticated client (AC) can see a listing

    # AC can follow the link directives from one node to another (read)

    # AC can create node content

    # AC can update node

    # AC can delete node (?)

    pass
