from django.shortcuts import get_object_or_404, render

from .models import Department

from rest_framework import viewsets
from .serializers import DeptSerializer


def index(request):
    latest_depts_list = Department.objects.order_by('abbreviation')
    return render(request, 'gus_depts/index.html', {'latest_depts_list': latest_depts_list})

def detail(request, dept_id):
    dept = get_object_or_404(Department, pk=dept_id)
    return render(request, 'gus_depts/detail.html', {'dept': dept})

class DeptViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows departments to be viewed or edited.
    """
    queryset = Department.objects.all()
    serializer_class = DeptSerializer
